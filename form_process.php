<!-- 

  Now that we created step 3 of the form, what’s left is the final
  processing script that inserts the data in the MySQL database.  

-->

<?php

# Turn off error reporting on "production"
error_reporting(0);

# Start or resume session
session_start();

$_SESSION["card_type"]         = $_POST["card_type"];
$_SESSION["card_number"]       = $_POST["card_number"];
$_SESSION["expiration_date"]   = $_POST["expiration_date"];
$_SESSION["verification_code"] = $_POST["verification_code"];

?>

</pre>


<?php

$db = sqlite_open('mydb.sqlite', 0666, $sqliteerror); 

# let's create the query
$insert_query = "INSERT INTO customer_details 
	VALUES (
		NULL,
		'" . $_SESSION['first_name'] . "',
		'" . $_SESSION['last_name'] . "',
		'" . $_SESSION['address'] . "',
		'" . $_SESSION['city'] . "',
		'" . $_SESSION['statel'] . "',
		'" . $_SESSION['phone'] . "',
		'" . $_SESSION['email'] . "'
);
";

# let's run the query
sqlite_query($db, $insert_query);

$insert_query = "INSERT INTO payment_details 
	VALUES (
		NULL,
		'" . $_SESSION['card_type'] . "',
		'" . $_SESSION['card_number'] . "',
		'" . $_SESSION['card_exp_date'] . "'
);
";

sqlite_query($db, $insert_query);
sqlite_close($db);

# Print out the results
print "The below session information was inserted into the database: ";
print "<pre>";
print_r($_SESSION);
print "</pre>";

?>
