<!--

  Source: http://www.html-form-guide.com/php-form/php-order-form.html

  Multi-page form using sessions

  In our example, we are going to create a three pages form, resembling a
  membership signup and payment form. The first page will ask for customer’s
  name and address, on the second page there is the choice for membership type,
  and on the third and final page, payment data must be entered. Final step is
  saving the data in MySQL.

  The first file we are going to create (step 1 of the form) will contain just a
  simple form with two fields.  

-->

<?php

# Turn off error reporting on "production"
error_reporting(0);

session_start();

# Initialize session variables
$_SESSION['name']              = "";
$_SESSION['email_address']     = "";
$_SESSION['first_name']        = "";
$_SESSION['last_name']         = "";
$_SESSION['address']           = "";
$_SESSION['city']              = "";
$_SESSION['state']             = "";
$_SESSION['phone']             = "";
$_SESSION['email']             = "";
$_SESSION['card_type']         = "";
$_SESSION['card_number']       = "";
$_SESSION['expiration_date']   = "";
$_SESSION['verification_code'] = "";

?>


<form method="post" action="form2.php">

   <br>First Name <input type="text" name="first_name">
   <br>Last Name  <input type="text" name="last_name">
   <br>Address    <input type="text" name="address">
   <br>City       <input type="text" name="city">
   <br>State      <input type="text" name="state">
   <br>Phone      <input type="text" name="phone">
   <br>Email      <input type="text" name="email">

   <br>           <input type="submit" value="Next">
</form>
