<!-- 

Ok, so nothing more than input fields and a submit button to take us to
step 2. In the following page, apart from the HTML form to gather membership
data, we are going to need code to store the submitted data from step 1 in the
session.  

-->

<?php

# Turn off error reporting on "production"
error_reporting(0);

# Start or resume session
session_start();

# Finally, let's store our posted values in the session variables
$_SESSION['name']              = $_POST['name'];
$_SESSION['email_address']     = $_POST['email_address'];
$_SESSION["first_name"]        = $_POST["first_name"];
$_SESSION["last_name"]         = $_POST["last_name"];
$_SESSION["address"]           = $_POST["address"];
$_SESSION["city"]              = $_POST["city"];
$_SESSION["state"]             = $_POST["state"];
$_SESSION["phone"]             = $_POST["phone"];
$_SESSION["email"]             = $_POST["email"];

?>

<form method="post" action="form_process.php">

 <br> Card Type                <input type="text" name="card_type">
 <br> Card Number              <input type="text" name="card_number">
 <br> Expiration Date          <input type="text" name="expiration_date">
 <br> Verification Code (CVV2) <input type="text" name="verification_code">

 <br>  <input type="submit" value="Next">

</form>
