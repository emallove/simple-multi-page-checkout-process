<!-- 

  Sample URL Request:

  HYPERLINK 

    http://www.infopay.com//phptest.php?username=accucomtest&password=test104&firstname=Steve&middle_initial=&lastname=Dunn&city=&state=ma&zip=&client_reference=test&phone=&housenumber=&streetname

  Final Notes

  When getting the files back to us at the end of the test, put in the email a 
  description of the approach you took, why you chose that approach, and what (if 
  anything) you would have done differently given more time.

  Now that we created step 3 of the form, what’s left is the final
  processing script that inserts the data in the MySQL database.  

-->

<?php

# Turn off error reporting on "production"
error_reporting(0);

$db = sqlite_open('mydb.sqlite', 0666, $sqliteerror); 

# let's create the query
$insert_query = "SELECT * FROM customer_details 
	WHERE 
		1
	";

if (isset($_GET['lastname'])) {
	$insert_query .= "\n AND last_name = '" . $_GET['lastname'] . "'";
}

if (isset($_GET['firstname'])) {
	$insert_query .= "\n AND first_name = '" . $_GET['first_name'] . "'";
}

if (isset($_GET['phone'])) {
	$insert_query .= "\n AND phone = '" . $_GET['phone'] . "'";
}

if (isset($_GET['state'])) {
	$insert_query .= "\n AND state = '" . $_GET['state'] . "'";
}

if (isset($_GET['streetname'])) {
	$insert_query .= "\n AND address = '" . $_GET['address'] . "'";
}

$result = sqlite_query($db, $insert_query);


# TODO: print in XML format, not plain text
print("<pre>\n");

# Ensure the <pre> tag is printed before the data
flush();

print(var_export(sqlite_fetch_all($result)));


# let's create the query
$insert_query = "SELECT * FROM payment_details;\n\n";
$result = sqlite_query($db, $insert_query);
print(var_export(sqlite_fetch_all($result)) . "\n\n</pre>");
print("\n\n</pre>");

sqlite_close($db);

?>
