DROP TABLE payment_details;
CREATE TABLE payment_details (
  id INT IDENTITY,
  card_type tinyint(1) NOT NULL,
  card_number varchar(50) NOT NULL,
  card_exp_date varchar(50) NOT NULL
);


DROP TABLE customer_details;
CREATE TABLE customer_details (
   id INT IDENTITY,
   first_name CHAR( 25 ) NOT NULL ,
   last_name CHAR( 25 ) NOT NULL ,
   address CHAR( 100 ) NOT NULL ,
   city CHAR( 50 ) NOT NULL ,
   state CHAR( 2 ) NOT NULL ,
   phone INT( 10 ) NOT NULL ,
   email VARCHAR( 100 ) NOT NULL
);

INSERT INTO customer_details VALUES ( null, '', '', 'm', 'm', '', '', '' ); 


